package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Calculator {
    static String simbols = "+-*/.";
    static String actions = "+-*/";
    static String simbolsAll = "+-*/.1234567890()";
    public static boolean checkDoubling(String str) {
        int len = str.length();
        String s = str.substring(len-1,len);
        if (simbols.contains(s)) {
            return false;
        }
        for (int i = 0 ; i < len ; i++) {
            s = str.substring(i,i+1);
            if (simbols.contains(s)) {
                String ss = str.substring(i+1,i+2);
                if (simbols.contains(ss)) {
                    return false;
                }
            }
        }
        return true;
    }
    public static boolean checkSim(String str) {
        int len = str.length();
        for (int i = 0 ; i < len ; i++) {
            String  s = str.substring(i,i+1);
            if (!simbolsAll.contains(s)) {
                return false;
            }
        }
        return true;
    }
    public static boolean checkBracket(String str) {
        int len = str.length();
        List<String> par = new ArrayList<String>();
        for (int i = 0 ; i< len ; i++) {
            String  s = str.substring(i,i+1);
            if (s.equals(")") || s.equals("(") ) {
                par.add(s);
            }
        }
        if (par.isEmpty()) {
            return true;
        }
        if (!par.get(0).equals("(")) {
            return false;
        }
        int leng = par.size();
        int a = leng +1;
        while (a != leng) {
            a=leng;
            for (int j =0 ; j < leng -1 ; j++) {
                if (par.get(j).equals("(") && par.get(j+1).equals(")")) {
                    par.remove(j);
                    par.remove(j);
                    leng = leng - 2;
                }
            }
        }
        return leng == 0;
    }
    public static String divisionMultiplication (String str) {
        int length = str.length();
        String strel;
        String fel="";
        String snel="";
        for (int i = 0 ; i < length ; i++ ) {
            strel = str.substring(i,i+1);
            if (strel.equals("/") || strel.equals("*")) {
                int k = i;
                while ( k>=1 && !actions.contains(str.substring(k-1,k)) ) {
                    k--;
                }
                if (k>0 && str.charAt(k - 1) == '-') {
                    fel = str.substring(k-1,i);
                }else fel = str.substring(k,i);
                int j = i;
                if (j<length - 1 && str.charAt(j + 1) != '-'){
                    while (j<length - 1 && !actions.contains(str.substring(j+1,j+2))) {
                        j++;
                    }
                }else {
                    while (j<length - 2 && !actions.contains(str.substring(j+2,j+3))) {
                        j++;
                    }
                }
                snel = str.substring(i+1,j+1);
                float rez= 0;
                if (strel.equals("/")) {
                    rez = Float.parseFloat(fel) /  Float.parseFloat(snel);
                    if(rez == Float.NEGATIVE_INFINITY || rez== Float.POSITIVE_INFINITY) {
                        str = "null";
                        break;
                    }
                }
                if (strel.equals("*")) {
                    rez = Float.parseFloat(fel) *  Float.parseFloat(snel);
                }
                String rezstr = String.valueOf(rez);
                if (k>0 && str.charAt(k - 1) == '-') {
                    str = str.replace(str.substring(k-1,j+1), rezstr);
                }else str = str.replace(str.substring(k,j+1), rezstr);
                length = str.length();
                i=0;
            }
        }
        return  str;
    }
    public static String additionSubtraction (String str) {
        int length = str.length();
        String strel;
        String fel="";
        String snel="";
        for (int i = 0 ; i < length ; i++ ) {
            strel = str.substring(i,i+1);
            if (strel.equals("+") || strel.equals("-") && i!=0) {
                int k = i;
                while ( k>=1 && !actions.contains(str.substring(k-1,k)) ) {
                    k--;
                }
                if (k>0 && str.charAt(k - 1) == '-') {
                    fel = str.substring(k-1,i);
                }else fel = str.substring(k,i);
                int j = i;
                while (j<length - 1 && !actions.contains(str.substring(j+1,j+2))) {
                    j++;
                }
                snel = str.substring(i+1,j+1);
                float rez= 0;
                if (strel.equals("+")) {
                    rez = Float.parseFloat(fel) +  Float.parseFloat(snel);
                }
                if (strel.equals("-")) {
                    rez = Float.parseFloat(fel) -  Float.parseFloat(snel);
                }
                String rezstr = String.valueOf(rez);
                if (k>0 && str.charAt(k - 1) == '-') {
                    str = str.replace(str.substring(k-1,j+1), rezstr);
                }else str = str.replace(str.substring(k,j+1), rezstr);
                length = str.length();
                i=0;
            }
        }
        return  str;
    }
    public static String total (String str) {
        int len = str.length();
        while (!Collections.disjoint(Arrays.asList(str.split("")), Arrays.asList(actions.split("")))) {
            String strcheck =str;
            for (int j =0 ; j < len ; j++) {
                if (str.charAt(j) == ')'){
                    for (int k = j ; k>=1 ; k--) {
                        if (str.charAt(k - 1) == '('){
                            String sub = str.substring(k,j);
                            sub = divisionMultiplication(sub);
                            if(sub.equals("null")) {
                                return null;
                            }
                            sub = additionSubtraction(sub);
                            str=str.replace(str.substring(k-1,j+1), sub);
                            str = doublSign(str);
                            len = str.length();
                            j=0;
                            break;
                        }
                    }
                }
                if (!str.contains(")")) break;
            }
            if (str.charAt(0) == '+') {
                str = str.substring(1,str.length());
            }
            str = doublSign(str);
            str = divisionMultiplication(str);
            str = doublSign(str);
            str = additionSubtraction(str);
            if (str.equals(strcheck)) break;
        }
        return str;
    }
    public static String doublSign (String str) {
        String check = str + "%";
        String str1 = str;
        while (!check.equals(str)) {
            check = str;
            str1 = str1.replace("--", "+");
            str1 = str1.replace("-+", "-");
            str1 = str1.replace("+-", "-");
            str1 = str1.replace("++", "+");
        }
        return str1;
    }

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement==null){
            return null;
        }
        statement =statement.replaceAll("\\s", "");
        if (statement.length()!=0 && checkDoubling(statement) && checkSim(statement) && checkBracket(statement) ) {
            statement= total(statement);
            if ("null".equals(statement)) {
                return null;
            }else {
                float f = Float.parseFloat(statement);
                if (f%1==0) {
                    return statement.substring(0,statement.length()-2);
                }else {
                    int num = Math.round(f*10000);
                    f = (float)num / 10000;
                    return String.valueOf(f);
                }
            }
        }else return null;
    }
}
