package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x==null || y==null){
            throw new IllegalArgumentException();
        }
        boolean b = true;
        int k = 0;

        Object elementX;
        Object elementY;
        for (int i = 0 ; i< x.size() ; i++) {
            if (b =true) {
                b= false;
                elementX = x.get(i);
                for (int j = k ;  j < y.size(); j++) {
                    elementY = y.get(j);
                    //System.out.println(i + "   "+ j + "   "+ b);
                    //System.out.println(elementX + "   "+elementY + "   "+ b);
                    try {
                        if (elementX.equals(elementY)) {
                            k = j +1;
                            b= true;
                            //System.out.println(elementX + "   "+elementY + "   "+ b);
                            break;
                        }

                    }
                    catch(Exception e) {
                        if (elementX ==elementY) {
                            k = j;
                            b= true;
                            //System.out.println(elementX + "   "+elementY + "   "+ b);
                            break;
                        }
                    }

                }
            }
            if (!b) {
                break;
            }
        }

        return b;
    }
}
