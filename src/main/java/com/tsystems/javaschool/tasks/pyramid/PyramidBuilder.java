package com.tsystems.javaschool.tasks.pyramid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        int length = inputNumbers.size();
        float  n = (float) ((-1 + Math.sqrt(1 + 8 * length)) / 2) ;
        if (n % 1 != 0) {
            throw new CannotBuildPyramidException();
        }

        for (int i = 0 ; i < inputNumbers.size() ; i++) {
            if (inputNumbers.get(i)==null) {
                throw new CannotBuildPyramidException();
            }
        }

        inputNumbers.sort((a,b) -> a-b);

        int k = (int) n;
        int lengthLine = k + k - 1;

        int[][] result = new int[k][lengthLine];

        int number = 0;
        for (int i = 0 ;  i < k ; i++) {
            int addition = 0;
            for (int j = 0 ; j<= i ; j++) {
                result[i][lengthLine/2 + (j - i) + addition] = inputNumbers.get(number);
                addition++;
                number++;

            }
        }

        return result;
    }
}
